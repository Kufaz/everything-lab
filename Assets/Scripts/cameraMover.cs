using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraMover : MonoBehaviour
{
    private bool isDragged;
    private Vector3 mouseInitialPos;
    //private Vector3 scaleChange;

    public void OnMouseDown()
    {
        isDragged = true;
        mouseInitialPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }

    public void OnMouseUp()
    {
        isDragged = false;
    }

    void Update()
    {
        if (isDragged)
        {
            Vector3 distance = mouseInitialPos - Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Camera.main.transform.position += distance;
        }
        Camera.main.orthographicSize = Mathf.Clamp((Camera.main.orthographicSize - (Input.GetAxis("Mouse ScrollWheel") * 16)),1,100);
        //Scales the camera mover object to camera ortho size ONLY WORKS AT 16x9!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        transform.localScale = new Vector3 (Camera.main.orthographicSize* 2.0f, Camera.main.orthographicSize * 2.0f, 1.0f);
    }
}
