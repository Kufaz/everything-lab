using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class placeObject : MonoBehaviour
{
    public Transform prefab;
    public GameObject collector;
    public int cost;

    public void OnMouseDown()
    {
        //get the number of coins from the collector's wallet
        collector wallet = collector.GetComponent<collector>();
        //spawns the object if you have enough coins
        if (wallet.wallet >= cost)
        {
        Transform t = Instantiate(prefab);
        t.localPosition = transform.position;
        //removes that number of coins from the wallet
        wallet.wallet -= cost;
        }
    }
}
