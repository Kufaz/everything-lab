using UnityEngine;
using UnityEngine.UI;

public class coinReader : MonoBehaviour
{

    public GameObject collector;
    public Text coins;

    // Update is called once per frame
    void Update()
    {
        collector wallet = collector.GetComponent<collector>();
        coins.text = wallet.wallet.ToString();

    }
}
