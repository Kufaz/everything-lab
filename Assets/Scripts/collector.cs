using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class collector : MonoBehaviour
{
    public int wallet;

    void OnCollisionEnter2D(Collision2D col)
    {
        //gets the Value of the object that collided with it
        resourceID collidedResource = col.gameObject.GetComponent<resourceID>();
        //adds that value to the wallet
        wallet += collidedResource.Value;
        //destroys the object that collided with it
        Destroy(col.gameObject);
    }
}