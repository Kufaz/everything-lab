using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class producer : MonoBehaviour
{
    public int resource0ID;
    public int resource1ID;
    bool hasResource1;
    bool hasResource2;
    public Transform spawnPrefab;
    public Vector3 spawnOffset;

    void OnCollisionEnter2D(Collision2D col)
    {
        //gets the ID number from the object that collided with it
        resourceID collidedResource = col.gameObject.GetComponent<resourceID>();
        int collidedID = collidedResource.ID;

        //Collects the resource if its ID matches an expected ID
        if (collidedID == resource0ID)
            {
            hasResource1 = true;
            Destroy(col.gameObject);
            }

        if (collidedID == resource1ID)
        {
            hasResource2 = true;
            Destroy(col.gameObject);
        }

        //spawns the new resource if both have been collected
        if (hasResource1 == true && hasResource2 == true)
        {
            Transform t = Instantiate(spawnPrefab);
            t.localPosition = transform.position + spawnOffset;
            hasResource1 = false;
            hasResource2 = false;
        }
    }  
}
