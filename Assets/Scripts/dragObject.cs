using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dragObject : MonoBehaviour {

    private bool isDragged;

    public void OnMouseOver()
    {
        if (Input.GetMouseButton(0))
        {
            isDragged = true;
        }
        else {
            isDragged = false;
        }
    }

    void Update()
    {
        if (isDragged) {
            Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
            transform.Translate(mousePosition);
            //Vector3 rotation = new Vector3 (0.0f,0.0f,0.1f);
            //transform.Rotate(rotation);

        }
    }
}
