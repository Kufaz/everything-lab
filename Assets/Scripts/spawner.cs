using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawner : MonoBehaviour{
    public Transform prefab;
    public float interval;
    float i = 0;
    
    void Update()
    {
        i += Time.deltaTime;
        if (i > interval)
        {
            Transform t = Instantiate(prefab);
            t.localPosition = transform.position;
            i = 0;
        }
    }
}
